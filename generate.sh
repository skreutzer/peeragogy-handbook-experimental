#!/bin/sh

java -cp ./automated_digital_publishing/html2epub/html2epub1/gui/html2epub1_config_metadata_editor/ html2epub1_config_metadata_editor ./metadata_html2epub2.xml

cd ./peeragogy-handbook/
cd ./Peeragogy.github.io/

grep -o "<a href=\"\./[^\"]*" index.html | sed -r "s/<a href=\"\.\/(.*).html/\1/" | xargs -I {} pandoc -o {}.html {}.md | pandoc -o workbook.html workbook.md
grep -o "<a href=\"\./[^\"]*" index.html | sed -r "s/<a href=\"\.\/(.*).html/\1/" | xargs -I {} pandoc -o {}.tex {}.html | pandoc -o workbook.tex workbook.html
cp ./*.html ../en/
cp ./*.tex ../en/

printf "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" > ../en/jobfile_html2epub2.xml
printf "<html2epub2-config>\n" >> ../en/jobfile_html2epub2.xml
printf "  <in>\n" >> ../en/jobfile_html2epub2.xml

# What about the problematic ALL.md? What about the missing workbook.md?
grep -o "<a href=\"\./[^\"]*" index.html | sed -r "s/<a href=\"\.\/(.*).html/\1/" | xargs -I %s printf "    <inFile title=\"%s\">%s.html</inFile>\n" >> ../en/jobfile_html2epub2.xml

printf "    <xhtmlSchemaValidation>true</xhtmlSchemaValidation>\n" >> ../en/jobfile_html2epub2.xml
printf "    <!-- javax.xml.stream.isValidating: Turns on/off implementation specific DTD validation. -->\n" >> ../en/jobfile_html2epub2.xml
printf "    <xhtmlReaderDTDValidation>false</xhtmlReaderDTDValidation>\n" >> ../en/jobfile_html2epub2.xml
printf "    <!-- javax.xml.stream.isNamespaceAware: Turns on/off namespace processing for XML 1.0 support. -->\n" >> ../en/jobfile_html2epub2.xml
printf "    <xhtmlReaderNamespaceProcessing>true</xhtmlReaderNamespaceProcessing>\n" >> ../en/jobfile_html2epub2.xml
printf "    <!-- javax.xml.stream.isCoalescing: Requires the processor to coalesce adjacent character data. -->\n" >> ../en/jobfile_html2epub2.xml
printf "    <xhtmlReaderCoalesceAdjacentCharacterData>true</xhtmlReaderCoalesceAdjacentCharacterData>\n" >> ../en/jobfile_html2epub2.xml
printf "    <!-- javax.xml.stream.isReplacingEntityReferences: replace internal entity references with their replacement text and report them as characters. -->\n" >> ../en/jobfile_html2epub2.xml
printf "    <!-- Always true! -->\n" >> ../en/jobfile_html2epub2.xml
printf "    <!-- javax.xml.stream.isSupportingExternalEntities: Resolve external parsed entities. -->\n" >> ../en/jobfile_html2epub2.xml
printf "    <xhtmlReaderResolveExternalParsedEntities>true</xhtmlReaderResolveExternalParsedEntities>\n" >> ../en/jobfile_html2epub2.xml
printf "    <!-- javax.xml.stream.supportDTD: Use this property to request processors that do not support DTDs. -->\n" >> ../en/jobfile_html2epub2.xml
printf "    <xhtmlReaderUseDTDNotDTDFallback>false</xhtmlReaderUseDTDNotDTDFallback>\n" >> ../en/jobfile_html2epub2.xml
printf "  </in>\n" >> ../en/jobfile_html2epub2.xml
printf "  <out>\n" >> ../en/jobfile_html2epub2.xml
printf "    <outDirectory>./e-book/</outDirectory>\n" >> ../en/jobfile_html2epub2.xml
printf "    <metaData/>\n" >> ../en/jobfile_html2epub2.xml
printf "  </out>\n" >> ../en/jobfile_html2epub2.xml
printf "</html2epub2-config>\n" >> ../en/jobfile_html2epub2.xml

cp -r ./images/ ../en/
cd ..
cd ..

cd ./peeragogy-handbook/
cd ./en/
xelatex peeragogy-shell.tex
biber peeragogy-shell.bcf
xelatex peeragogy-shell.tex
xelatex peeragogy-shell.tex
cp peeragogy-shell.pdf ../../
cd ..
cd ..

for file in ./peeragogy-handbook/en/*.html
do
    printf "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" > ./jobfile_text_concatenator_1.xml
    printf "<text-concatenator-1-job>\n" >> ./jobfile_text_concatenator_1.xml
    printf "  <input>\n" >> ./jobfile_text_concatenator_1.xml
    printf "    <text-file path=\"./xhtml_pre.txt\"/>\n" >> ./jobfile_text_concatenator_1.xml
    printf "    <text-file path=\"%s\"/>\n" $file >> ./jobfile_text_concatenator_1.xml
    printf "    <text-file path=\"xhtml_post.txt\"/>\n" >> ./jobfile_text_concatenator_1.xml
    printf "  </input>\n" >> ./jobfile_text_concatenator_1.xml
    printf "  <output-file path=\"%s.tmp\"/>\n" $file >> ./jobfile_text_concatenator_1.xml
    printf "</text-concatenator-1-job>\n" >> ./jobfile_text_concatenator_1.xml

    java -cp ./digital_publishing_workflow_tools/text_concatenator/text_concatenator_1/ text_concatenator_1 jobfile_text_concatenator_1.xml resultinfo_text_concatenator_1.xml
    mv $file.tmp $file
    rm jobfile_text_concatenator_1.xml
done

java -cp ./automated_digital_publishing/html2epub/html2epub1/workflows/ html2epub1_config_merge1 ./peeragogy-handbook/en/jobfile_html2epub2.xml ./metadata_html2epub2.xml ./peeragogy-handbook/en/jobfile_html2epub2.xml
mkdir -p ./peeragogy-handbook/en/e-book/
java -cp ./automated_digital_publishing/html2epub/html2epub2/ html2epub2 ./peeragogy-handbook/en/jobfile_html2epub2.xml
# epubcheck
cp ./peeragogy-handbook/en/e-book/out.epub .

